import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  //varför static här?
  static email(control){
    return control.value.match(/(.)+@(.)+\.(.)+/) ? null : {invalidEmail: true};
  }
  constructor() { }
}
