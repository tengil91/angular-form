import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InviteFormComponent } from './invite-form/invite-form.component';
import { GuestListComponent } from './guest-list/guest-list.component';
import { GreetingComponent } from './greeting/greeting.component';


const routes: Routes = [
  {
    path: 'form',
    component: InviteFormComponent
  },
  {
    path: 'guests',
    component: GuestListComponent
  },
  {
    path: '',
    component: GreetingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
