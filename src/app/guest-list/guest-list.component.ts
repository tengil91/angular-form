import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-guest-list',
  templateUrl: './guest-list.component.html',
  styleUrls: ['./guest-list.component.css']
})
export class GuestListComponent implements OnInit {

  name = null;
  email = null;
  food = null;
  company = null;

  constructor(private activeatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeatedRoute.queryParams.subscribe(params => {
      this.name = params.name;
      this.email = params.email;
      this.food = params.food;
      this.company = params.company;
    });
  }

}
