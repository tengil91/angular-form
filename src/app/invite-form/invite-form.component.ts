import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormValidationService } from '../form-validation.service';

@Component({
  selector: 'app-invite-form',
  templateUrl: './invite-form.component.html',
  styleUrls: ['./invite-form.component.css']
})
export class InviteFormComponent implements OnInit {

  submited = false;
  inviteFormData = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
    email: ['', [Validators.required, FormValidationService.email]],
    company: [''],
    food: ['', [Validators.required]]
  });

  constructor(private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void { }

  sendForm(){
    this.submited = true;
    console.log(this.inviteFormData);
    if(this.inviteFormData.valid){
      console.log("valid :)");
      this.router.navigate(['guests'], {
        queryParams: {
          name: this.inviteFormData.value.name,
          email: this.inviteFormData.value.email,
          food: this.inviteFormData.value.food,
          company: this.inviteFormData.value.company,
        }
      });
    } else {
      console.log('inte valid :(');
    }
  }

}
